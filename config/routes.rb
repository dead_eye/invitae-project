Rails.application.routes.draw do

#  root :to => 'application/index'
  get 'invitae/index' => 'invitae#index'

  namespace :api do
    match 'variants/autofill', to: 'variants#autofill', via: [:get, :post]
    match 'variants/search', to: 'variants#search', via: [:get, :post]

    resources :variants
  end
  root 'invitae#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
