require 'logger'

class  Api::VariantsController < ApplicationController


  logger = Logger.new(STDOUT)
  logger.level = Logger::WARN

  skip_before_action :verify_authenticity_token

  def search
    logger.info "search"

    search = params['searchStr'].nil? ? '' : params['searchStr'].upcase.strip
    logger.info(":::: search:: #{search}")

    searchResults = Variants.instance.search(search)

    render plain: searchResults.to_json, content_type: 'application/json'

  end

  def autofill
    logger.info "autoFill"

    logger.info(":::: #{params}")

    search = params['searchStr'].nil? ? '' : params['searchStr'].upcase.strip
    logger.info(":::: search:: #{search}")

    render plain: Variants.instance.autoFill(search), content_type: 'application/json'

  end



end
