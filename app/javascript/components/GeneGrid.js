import React from "react"
import PropTypes from "prop-types"

import { Grid, Header, Segment } from 'semantic-ui-react'

export default class GeneGrid extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            searchResults: []
        }
    }

    componentWillReceiveProps(nextProps) {
         this.setState({searchResults: nextProps.searchResults})
    }

    render() {


        if(this.state.searchResults == null || this.state.searchResults.length == 0) {
           return ""
        }
//Other Mappings
//

        /*
         <Grid.Column>{gene['Alias']}</Grid.Column>
         <Grid.Column>{gene['Transcripts']}</Grid.Column>
         <Grid.Column>{gene['Region']}</Grid.Column>
         <Grid.Column>{gene['Reported Classification']}</Grid.Column>
         <Grid.Column>{gene['Inferred Classification']}</Grid.Column>
         <Grid.Column>{gene['Submitter Comment']}</Grid.Column>
         <Grid.Column>{gene['Assembly']}</Grid.Column>
         <Grid.Column>{gene['Chr']}</Grid.Column>
         <Grid.Column>{gene['Ref']}</Grid.Column>
         <Grid.Column>{gene['Alt']}</Grid.Column>
         <Grid.Column>{gene['Accession']}</Grid.Column>
         <Grid.Column width={3}>{gene['Reported Ref']}</Grid.Column>
         <Grid.Column width={3}>{gene['Reported Alt']}</Grid.Column>
         <Grid.Column width={2}>{gene['Last Evaluated']}</Grid.Column>

         */
        return (
            <Grid columns={11}>
                <Grid.Row color="blue">
                    <Grid.Column width={2}>Gene</Grid.Column>
                    <Grid.Column width={3}>Nucleotide Change</Grid.Column>
                    <Grid.Column width={2}>Protein Change</Grid.Column>
                    <Grid.Column width={1}>Source</Grid.Column>
                    <Grid.Column width={2}>Last Updated</Grid.Column>
                    <Grid.Column width={1}>URL</Grid.Column>
                    <Grid.Column width={2}>Genomic Start</Grid.Column>
                    <Grid.Column width={2}>Genomic Stop</Grid.Column>
                </Grid.Row>
                    {
                    this.props.searchResults.map((gene, index) =>
                        <Grid.Row>
                            <Grid.Column width={2}>{gene['Gene']}</Grid.Column>
                            <Grid.Column width={3}>{gene['Nucleotide Change']}</Grid.Column>
                            <Grid.Column width={2}>{gene['Protein Change']}</Grid.Column>
                            <Grid.Column width={1}>{gene['Source']}</Grid.Column>
                            <Grid.Column width={2}>{gene['Last Updated']}</Grid.Column>
                            <Grid.Column width={1}><a href={gene['URL']} target="_blank">URL</a></Grid.Column>
                            <Grid.Column width={2}>{gene['Genomic Start']}</Grid.Column>
                            <Grid.Column width={2}>{gene['Genomic Stop']}</Grid.Column>
                        </Grid.Row>
                    )
                }
            </Grid>

        )
    }
}


