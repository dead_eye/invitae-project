import _ from 'lodash'
import React from "react"
import PropTypes from "prop-types"

import { Search, Button } from 'semantic-ui-react'

import GeneGrid from './GeneGrid'


class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            "isLoading": false,
            "autoFillResults":[],
            "searchResults": [],
            "value": "",
            "searchButton": "Search!",
            "searchButtonDisabled" : false
        }
        this.handleAutoFillChange = this.handleAutoFillChange.bind(this)
        this.handleSearchButton = this.handleSearchButton.bind(this)
        this.handleResultSelect = this.handleResultSelect.bind(this)
    }

    handleSearchButton(e) {
        this.search(this.state.value.trim())
        this.clearResults()
    }


    handleResultSelect(e, result)
    {
        this.search(result.result.title)
        this.clearResults()
    }

    search(searchMe) {
        this.setState({
            searchButton: "Loading...",
            searchButtonDisabled: true
        })

        fetch("/api/variants/search",
            {
                method: 'POST',
                headers: {'CONTENT-TYPE':'APPLICATION/JSON'},
                body: JSON.stringify({
                    searchStr: searchMe
                })
            }
        ).then(res => res.json())
            .then((searchResults) => {
                    this.setState({
                        isLoading:false,
                        searchResults,
                        searchButton: "Search!",
                        searchButtonDisabled: false
                    })

                }, (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                        autoFillResults:[],
                        searchResults: [],
                        value: "",
                        searchButton: "Search!",
                        searchButtonDisabled: false
                    });
                }
            )


    }

    clearResults() {
        this.setState({value:"",
            autoFillResults:[],
            searchResults: []
        })

    }

    handleAutoFillChange(e, value) {
        console.log("value::" + value.value)

        if (value.value == null || value.value.length == 0) {
            this.clearResults()
            return
        }
        this.setState({
            isLoading: true,
            value: value.value,
        })
            fetch("/api/variants/autofill",
                {
                    method: 'POST',
                    headers: {'CONTENT-TYPE':'APPLICATION/JSON'},
                    body: JSON.stringify({
                        searchStr: value.value
                    })
                }

            )
                .then(res => res.json())
                .then(
                    (result) => {
                        var processedResults = []
                        for ( var i=0;i< result.length; i++) {
                            processedResults.push({title: result[i]})
                        }

                        this.setState({
                            isLoading:false,
                            autoFillResults: processedResults
                        })

                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error,
                            autoFillResults:[],
                            searchResults: [],
                            value: ""
                        });
                    }
                )
    }


    render() {
        const { isLoading, value, autoFillResults, searchResults, searchButtonDisabled } = this.state
        return (
            <div style={{padding:"20px;"}}>
                <div style={{padding:"10px", float : "left"}}>
                    <Search
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={this.handleAutoFillChange}
                    results={autoFillResults}
                    value={value}
                    {...this.props}
                    />
                </div>
                <div style={{padding:"10px", float : "left"}}>
                    <Button disabled={searchButtonDisabled} onClick={this.handleSearchButton}>{this.state.searchButton}</Button>
                </div>
                <div style={{padding:"10px", clear : "both"}}>
                    <GeneGrid searchResults={searchResults}/>
                </div>
            </div>
        );
    }
}

export default App
