import React from 'react';
import { mount } from 'enzyme';
import GeneGrid from './GeneGrid'


describe('GeneGrid', () => {
    describe('when GeneGrid receives searchResults', () => {
        it('should render children', () => {
            const wrapper = mount(
                <GeneGrid searchResults={[]}/>
            )
            expect(wrapper.html()).toEqual('<div>ahoy!</div>');
            wrapper.unmount();
        });
    });
});