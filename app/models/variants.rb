class Variants

  def initialize
    if @genes.nil?

      @names = SortedSet.new

      tsv = StrictTsv.new("config/variants.tsv")
      @genes = []
      @letterHash = Hash.new
      tsv.parse do |row|
        @genes.push row
        if !row["Gene"].nil?
          @names << row["Gene"]
        end
      end
      @genes.sort_by! { |row| row["Gene"] }


      @names.each { |name|
        if !name.nil?
          letterArray = @letterHash[name[0]]
          if letterArray.nil?
            letterArray = Array.new
            @letterHash[name[0]] = letterArray
          end
          letterArray.push(name)
        end
      }
    end
  end


  def self.instance

    if @@instance.nil?
      @@instance = Variants.new
    end

    return @@instance
  end

  @@instance = Variants.new


  def autoFill(search)
    if !search.nil? && search.length == 0
      return []
    end
    letterArray = @letterHash[search[0]]

    if search.length == 1
      return letterArray
    end

    selectedNames = letterArray.select { |geneName|
      geneName.start_with?(search.upcase)
    }
    return selectedNames
  end

  def search(search)

    if search == ''
      return []
    end
    searchMe = search.upcase.strip
    searchedGenes = @genes.select {|gene| gene["Gene"].starts_with?(searchMe) }

    return searchedGenes
  end

  private_class_method :new

end
