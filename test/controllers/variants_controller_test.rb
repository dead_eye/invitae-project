require 'test_helper'

class VariantsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  test "autofill up" do
    post "/api/variants/autofill", params: { searchStr: 'a' }


    searchJson = JSON.parse(@response.body)
    #Rails::logger.debug ":::::#{searchJson.length}"

    assert_response :success
    assert searchJson.length == 467
  end

  test "autofill not found" do
    post "/api/variants/autofill", params: { searchStr: 'aasdf' }


    searchJson = JSON.parse(@response.body)
    #Rails::logger.debug ":::::#{searchJson.length}"

    assert_response :success
    assert searchJson.length == 0
  end


  test "search up" do
    post "/api/variants/search", params: { searchStr: 'aaa' }


    searchJson = JSON.parse(@response.body)
    Rails::logger.debug ":::::#{searchJson.length}"

    assert_response :success
    assert searchJson.length == 7
  end


  test "search not found" do
    post "/api/variants/search", params: { searchStr: 'asdgfhfdhsd' }


    searchJson = JSON.parse(@response.body)
    Rails::logger.debug ":::::#{searchJson.length}"

    assert_response :success
    assert searchJson.length == 0
  end

end
