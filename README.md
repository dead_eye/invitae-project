# README

William Chen

The project was developed with ruby 2.4.1, but it's fine to change the gemfile to ruby 2.5.1

git clone https://dead_eye@bitbucket.org/dead_eye/invitae-project.git

cd into invitae-project
>> bundle install
>> yarn install
>> rails s

to run the rails tests that check the api:
>> bin/rails test test

to run the jest/enzyme test:
>> yarn test

The jest/enzyme tests honestly don't do anything, it's only partially setup.  I wasn't able to crack why
'import' was causing a problem "SyntaxError: Unexpected token import", but I suspect my babel setup wasn't
quite right.

open up a web browser to http://locahost:3000/

The project is deployed on the internet @ http://157.230.160.110/

The API is also accesible via http://157.230.160.110/api/variants/autofill/?searchStr=eg and
http://157.230.160.110/api/variants/search/?searchStr=eg

